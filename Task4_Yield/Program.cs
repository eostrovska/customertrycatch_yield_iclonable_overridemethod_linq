﻿using System;

namespace Task4_Yield
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] studentArray = new Student[]{ new Student(true, true, "Kate"), new Student(true, true, "Ben"), new Student(true, false, "Nick"),
                                                    new Student(false, false, "Luk"), new Student(false, true, "Dinn")};

            Console.WriteLine("Task 4: Create a factory method that returns a collection of students who attend classes and do homework. Use yeild");
            Console.WriteLine(new string('-', 60));

            Student.GetGoodStudentCollection(studentArray);

            Console.ReadKey();
        }
    }
}
