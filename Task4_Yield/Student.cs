﻿using System;
using System.Collections;

namespace Task4_Yield
{
    class Student
    {
        private bool isHomeTasksDone;
        private bool isStudentAttend;
        private string name;

        public string Name { get { return name; } }

        public Student(bool isHomeTasksDone, bool isStudentAttend, string name)
        {
            this.isHomeTasksDone = isHomeTasksDone;
            this.isStudentAttend = isStudentAttend;
            this.name = name;
        }

        public static IEnumerable GetGoodStudentCollection(Student[] studentArray)
        {
            for (int i = 0; i < studentArray.Length; i++)
            {
                if (studentArray[i].isHomeTasksDone == true && studentArray[i].isStudentAttend == true)
                {
                    yield return studentArray[i];
                }
            }
        }
    }
}
