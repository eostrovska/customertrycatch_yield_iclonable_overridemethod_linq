﻿using System;

namespace HomeTasks_Lesson11
{
    class Product
    {
        private string type;
        private float price;
        private string name;

        public string Type { get { return type; } }
        public float Price { get { return price; } }
        public string Name { get { return name; } }

        public Product(string type, float price, string name)
        {
            this.type = type;
            this.price = price;
            this.name = name;
        }

        public override bool Equals(Object product)
        {
            if (product == null || this.GetType() != product.GetType())
            {
                return false;
            }

            Product equalsProduct = (Product)product;
            return (type == equalsProduct.type) && (price == equalsProduct.price) && (name == equalsProduct.name);
        }

        public override int GetHashCode()
        {
            var hash = 25;
            hash = hash * 39 + type.GetHashCode();
            hash = hash * 39 + price.GetHashCode();
            hash = hash * 39 + name.GetHashCode();
            return hash;
        }
    }
}
