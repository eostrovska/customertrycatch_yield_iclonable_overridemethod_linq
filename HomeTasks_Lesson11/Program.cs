﻿using HomeTasks_Lesson11;
using System;

namespace Task1_OverrideMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            Product banana = new Product("Fruit", 23, "Banana");
            Product banana1 = new Product("Fruit", 23, "Banana");
            Product peach = new Product("Fruit", 56.45F, "Peach");

            Console.WriteLine("Task 1 Create a Tovar class with the required fields." +
                              "Override the Equals and GetHashCode methods, so that if the goods of the same name, type and price are equal.");
            Console.WriteLine(new string('-', 60));
            Console.WriteLine(banana.GetHashCode());
            Console.WriteLine(banana1.GetHashCode());
            Console.WriteLine(banana.Equals(banana1));
            Console.WriteLine(peach.Equals(banana1));

            Console.ReadKey();
        }
    }
}
