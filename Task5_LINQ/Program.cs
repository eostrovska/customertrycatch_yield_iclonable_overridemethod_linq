﻿using System;
using System.Linq;

namespace Task5_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            Product[] productStore = new Product[]
                                    {
                                        new Product(78, "Banana", "Fruit", 30),
                                        new Product(456, "Peach", "Fruit", 56),
                                        new Product(67890, "Rice", "Cereals", 45)
                                    };

            Console.WriteLine("Task 5: Create a collection of products with the following data:" +
                              "product code, product name, product type, price." +
                              "Using the LINQ query, display the product code with a price above 30.");
            Console.WriteLine(new string('-', 60));

            var productWith30Price =
                from product in productStore
                where product.Price == 30
                select new
                {
                    ProductCode = product.ProductCode,
                };

            foreach (var product in productWith30Price)
            {
                Console.Write("The code of product is {0} ", product.ProductCode);
            }

            Console.ReadKey();
        }
    }
}
