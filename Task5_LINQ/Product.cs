﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5_LINQ
{
    class Product
    {
        private int productCode;
        private string productName;
        private string productType;
        private float price;

        public int ProductCode { get { return productCode; } }
        public string ProductName { get { return productName; } }
        public string ProductType { get { return productType; } }
        public float Price { get { return price; } }

        public Product(int productCode, string productName, string productType, float price)
        {
            this.productCode = productCode;
            this.productName = productName;
            this.productType = productType;
            this.price = price;
        }
    }
}
