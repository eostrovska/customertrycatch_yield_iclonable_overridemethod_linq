﻿using System;

namespace Task2_IClonable
{
    class Program
    {
        static void Main(string[] args)
        {
            Sheep[] sheepArray = new Sheep[] { new Sheep(12), new Sheep(23.5F), new Sheep(18.45F), new Sheep(45), new Sheep(34) };

            Console.WriteLine("Task 2: You have an array of instances of the class Sheep." +
                              "The sheep have a field weight and ect. Using bubble sorting, to sort the array by weight" +
                              "The sheep class implements ICloneable interface.");
            Console.WriteLine(new string('-', 70));

            BubbleSort(sheepArray);

            for (int i = 0; i < sheepArray.Length; i++)
            {
                Console.WriteLine(sheepArray[i].Weight);
            }

            Console.ReadKey();
        }

        static void BubbleSort(Sheep[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length - 1; j++)
                {
                    if (array[j].Weight > array[j + 1].Weight)
                    {
                        float temp = array[j + 1].Weight;
                        array[j + 1].Weight = array[j].Weight;
                        array[j].Weight = temp;
                    }
                }
            }
        }
    }
}
