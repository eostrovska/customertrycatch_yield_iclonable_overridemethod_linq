﻿using System;

namespace Task2_IClonable
{
    class Sheep : ICloneable
    {
        private float weight;

        public float Weight { get { return weight; } set { weight = value; } }

        public Sheep(float weight)
        {
            this.weight = weight;
        }

        public object Clone()
        {
            return new Sheep(this.weight) as object;
        }
    }
}
