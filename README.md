### What is this repository for? ###

* Task 1 Create a Tovar class with the required fields.
  Override the Equals and GetHashCode methods, so that if the goods of the same name, type and price are equal.
* Task 2: You have an array of instances of the class Sheep.
  The sheep have a field weight and ect. Using bubble sorting, to sort the array by weight
  The sheep class implements ICloneable interface.
* Task 3: Create a static class ExceptionFactory. It has factory methods for instantiating various exceptions (dividing by zero, etc).
  In the Main method make one try and a lot of catch. Each catch is intended to catch its instance of an exception.
* Task 4: Create a factory method that returns a collection of students who attend classes and do homework. Use yeild
* Task 5: Create a collection of products with the following data:
  product code, product name, product type, price.
  Using the LINQ query, display the product code with a price above 30.

