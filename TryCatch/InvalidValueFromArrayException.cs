﻿using System;

namespace TryCatch
{
    class InvalidValueFromArrayException : Exception
    {
        private int errorCode;
        private string proposedAction;

        public int ErrorCode { get { return errorCode; } }
        public string ProposedAction { get { return proposedAction; } }

        public InvalidValueFromArrayException(string aMessage, int errorCode, string proposedAction)
        : base(aMessage)
        {
            this.errorCode = errorCode;
            this.proposedAction = proposedAction;
        }
    }
}
