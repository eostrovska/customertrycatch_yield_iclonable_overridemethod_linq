﻿using System;

namespace TryCatch
{
    class PersonException : Exception
    {
        private int errorCode;

        public int ErrorCode { get { return errorCode; } }

        public PersonException(string aMessage, int aCode)
        : base(aMessage)
        {
            errorCode = aCode;
        }
    }
}
