﻿using System;
using TryCatch;


namespace Task3_TryCatch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 3: Create a static class ExceptionFactory. It has factory methods for instantiating various exceptions (dividing by zero, etc)." +
                             "In the Main method make one try and a lot of catch. Each catch is intended to catch its instance of an exception.");
            Console.WriteLine(new string('-', 60));
            string[] stringArray = new string[3] { "First", "Second", "Third" };
            try
            {
                //ExceptionFactory.SafeDivision(23d, 0d);
                //ExceptionFactory.RegistrationNewUser(13);
                ExceptionFactory.GetValueFromArray(stringArray, "Fourth");
            }
            catch (DevisionByZeroExceptions ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }
            catch (PersonException ex)
            {
                Console.WriteLine("Error #:{0}. {1}", ex.ErrorCode, ex.Message);
            }
            catch (InvalidValueFromArrayException ex)
            {
                Console.WriteLine("Error #:{0}. {1}. Propose:{2}", ex.ErrorCode, ex.Message, ex.ProposedAction);
            }

            Console.ReadKey();
        }
    }
}
