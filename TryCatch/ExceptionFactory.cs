﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatch
{
    public static class ExceptionFactory
    {
        public static double SafeDivision(double x, double y)
        {
            if (y == 0)
                throw new DevisionByZeroExceptions("Devision by zero is impossible!");
            return x / y;
        }

        public static string RegistrationNewUser(byte age)
        {
            string correctRegistrationMessage = "Confirmation of registration";

            if (age < 18)
                throw new PersonException("Registration is allowed only for persons over 18 years of age", 3489);
            return correctRegistrationMessage;
        }

        public static string GetValueFromArray(string[] array, string value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != value)
                    throw new InvalidValueFromArrayException("Index out of range exception", 3489, "Add new array element");

            }
            return value;
        }
    }
}
