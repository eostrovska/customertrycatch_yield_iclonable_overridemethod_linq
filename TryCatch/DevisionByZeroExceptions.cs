﻿using System;

namespace TryCatch
{
    class DevisionByZeroExceptions : Exception
    {
        public DevisionByZeroExceptions(string aMessage)
        : base(aMessage)
        {
        }
    }
}
